package ent.java;

public class ItemScene {
    protected int x, y; // Coordonnées (pixels) dans la scène : colonne, ligne
    protected int dx, dy; // Déplacements instantanés (pixels) de l'item
    protected Sprite sprite;

    public ItemScene(String cheminImage, int x, int y, int dx, int dy) {
        this.sprite = new Sprite(cheminImage);
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDx() {
        return dx;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public int getDy() {
        return dy;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public void deplacer() { // Applique les changements de position
        x = x + dx;
        y = y + dy;
    }

    public void actionCollision() {
    }
}