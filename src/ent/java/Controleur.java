package ent.java;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Controleur implements KeyListener {
    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT) {
            Krashblok.joueur.deplacer("G"); // Met à jour le modèle
        }
        else if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT) {
            Krashblok.joueur.deplacer("D"); // Met à jour le modèle
        }
        else if (keyEvent.getKeyCode() == KeyEvent.VK_ESCAPE) {
            // TODO : gérer la pause dans le jeu ?
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}
