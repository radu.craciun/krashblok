package ent.java;

public class Obstacle extends ItemScene {
    public Obstacle(String cheminImage, int x, int y, int dx, int dy) {
        super(cheminImage, x, y, dx, dy);
//        this.largeur = this.image.getWidth(null); // TODO : supprimer
//        this.largeur = this.image.getHeight(null);
    }

//    public Obstacle(int x, int y, int dx, int dy) {
//        // TODO : Choisir un obstacle aléatoire
//    }

    @Override
    public void actionCollision() {
        Krashblok.joueur.gagneOuPerdVies(-1);
        Krashblok.joueur.revenirAuDepart();
        if (Krashblok.joueur.getVies() == 0) { // Au cas où la collision est fatale
            Krashblok.estPartieFinie = true;
        }
    }
}