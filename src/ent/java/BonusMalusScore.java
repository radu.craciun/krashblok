package ent.java;

public class BonusMalusScore extends ItemScene { // Donne ou enlève des points
    protected int montant; // Entier relatif

    public BonusMalusScore(String cheminImage, int x, int y, int dx, int dy, int montant) {
        super(cheminImage, x, y, dx, dy);
        this.montant = montant;
    }

//    public BonusMalusScore(String cheminImage, int x, int y, int dx, int dy) {
//        this(cheminImage, x, y, dx, dy);
//        this.montant = montant; // TODO : Fixer un montant aléatoire
//    }

    @Override
    public void actionCollision() {
        Krashblok.joueur.gagneouPerdPoints(this.montant);
    }
}