package ent.java;

import javax.swing.*;

public class Krashblok {
    public static JFrame fenetre;
    public static Scene scene; // Scène du jeu
    public static JPanel infos;
    public static Joueur joueur; // Dépend de la fenêtre pour placer le sprite
    public static boolean estPartieFinie;

    public static void main(String[] args) {
        fenetre = new JFrame("KrashBloK");
        fenetre.setSize(640, 480);
        fenetre.setLocationRelativeTo(null); // Centre la fenêtre
        fenetre.setResizable(false); // Sinon les images risquent de sortir de l'écran
        fenetre.setAlwaysOnTop(true); // Evite de perdre la partie en cliquant ailleurs sur l'écran
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        scene = new Scene(); // Doit être instanciée avant le joueur
        infos = new JPanel();
        infos.setOpaque(false); // Panneau transparent

        joueur = new Joueur(); // Doit être instancié après la scène
        fenetre.setContentPane(scene);

        estPartieFinie = false;

        fenetre.setVisible(true);
    }


}
