package ent.java;

import java.util.Random;

public class Metronome implements Runnable {
    Random r = new Random();

    @Override
    public void run() {
        while (!Krashblok.estPartieFinie) {
            Krashblok.scene.getItemJoueur().deplacer(); // Met à jour la vue
            Krashblok.joueur.calculScore();
            Krashblok.scene.testCollision();
            Krashblok.scene.testNiveauSuivant();
            Krashblok.scene.repaint(); // Affiche la vue
            try {
                int INTERVALLE = 150;
                Thread.sleep(INTERVALLE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
