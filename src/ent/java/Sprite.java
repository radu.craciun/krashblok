package ent.java;

import javax.swing.*;
import java.awt.*;

public class Sprite {
    //protected ImageIcon icone; // TODO : encapsuler la partie affichage dans un sprite
    protected Image image;
    //protected String cheminImage;
    protected int largeur, hauteur;

    public Sprite(String cheminImage) {
        //this.cheminImage = cheminImage;
        //this.icone = new ImageIcon(getClass().getResource(this.cheminImage));
        //this.image = this.icone.getImage();
        this.image = new ImageIcon(getClass().getResource(cheminImage)).getImage();
        this.largeur = this.image.getWidth(null);
        this.hauteur = this.image.getHeight(null);
    }

    public Image getImage() {
        return image;
    }

    public int getLargeur() {
        return largeur;
    }

    public int getHauteur() {
        return hauteur;
    }
}
