package ent.java;

public class Joueur {
    private int score;
    private int vies;
    private final ItemScene itemJoueur;

    public Joueur() {
        this.score = 0; // Score initial
        this.vies = 3; // 3 vies pour commencer
        this.itemJoueur = Krashblok.scene.getItemJoueur();
    }

    public void deplacer(String direction) { // La chute est implicite
        if (direction.equals("G")) {
            if (itemJoueur.getX() >= itemJoueur.getSprite().getLargeur()) {
                itemJoueur.setDx(-5);
            }
        }
        else if (direction.equals("D")) {
            if (itemJoueur.getX() <= Krashblok.fenetre.getWidth() - itemJoueur.getSprite().getLargeur()) {
                itemJoueur.setDx(5);
            }
        }
//        itemScene.setX(itemScene.getX() + itemScene.getDx()); // x = x + dx : mise à jour de la position
        itemJoueur.deplacer(); // Applique changement de position, incluant la chute
        itemJoueur.setDx(0); // Pour ne déplacer que d'un seul incrément par appui de touche
    }

    public int getScore() {
        return score;
    }

    public int getVies() {
        return vies;
    }

    public ItemScene getItemJoueur() {
        return itemJoueur;
    }

    public void calculScore() {
        this.score += this.itemJoueur.getDy() * Krashblok.scene.getNiveau(); // Score augmenté de l'incrément vertical
    }

    public void gagneouPerdPoints(int montant) {
        score += montant;
    }

    public void gagneOuPerdVies(int montant) {
        vies += montant;
    }

    public void collisionAvecItem(ItemScene item) {
        if (item.getX() <= itemJoueur.getX() + itemJoueur.getSprite().getLargeur() && itemJoueur.getX() <= item.getX() + item.getSprite().getLargeur()
         && item.getY() <= itemJoueur.getY() + itemJoueur.getSprite().getHauteur() && itemJoueur.getY() <= (item.getY() + item.getSprite().getHauteur())) {
            item.actionCollision();
        }
    }

    public void revenirAuDepart() {
        this.itemJoueur.setX(Krashblok.scene.getFond().getLargeur()/2);
        this.itemJoueur.setY(0);
    }
}