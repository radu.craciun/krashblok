package ent.java;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Scene extends JPanel {
    private Sprite fond;
    private ItemScene itemJoueur;
    private ArrayList<ItemScene> items;
    private int niveau;

    public Scene() {
        super();
        this.fond = new Sprite("img/fond.png");
        this.itemJoueur = new ItemScene("img/joueur.png",
                Krashblok.fenetre.getWidth()/2,0, // Centre de l'écran
                0,5); // Déplacement nominal
        this.setFocusable(true);
        this.requestFocusInWindow(); // La fenêtre réagit avec le clavier
        this.addKeyListener(new Controleur()); // Ajoute un contrôleur à la scène

        this.items = new ArrayList<>();
        this.ajouterItems(5, this.fond.getLargeur() - this.itemJoueur.getSprite().getLargeur(), this.fond.getHauteur() - this.itemJoueur.getSprite().getHauteur());

        this.niveau = 1;
        Thread chute = new Thread(new Metronome()); // Chute du bloc du joueur
        chute.start();
    }

    public int getNiveau() {
        return niveau;
    }

    public ItemScene getItemJoueur() {
        return itemJoueur;
    }

    public Sprite getFond() {
        return fond;
    }

    public void paintComponent(Graphics g) {
        g.drawImage(this.fond.getImage(), 0, 0, null);
        g.drawImage(this.itemJoueur.getSprite().getImage(), this.itemJoueur.getX(), this.itemJoueur.getY(), null);
        for (ItemScene item : items) {
            g.drawImage(item.getSprite().getImage(), item.getX(), item.getY(), null);
        }
    }

    public void testCollision() {
        for (ItemScene item : items) {
            Krashblok.joueur.collisionAvecItem(item);
        }
    }

    public void testNiveauSuivant() {
        if (itemJoueur.getY() >= this.fond.getHauteur()) {
            Krashblok.joueur.revenirAuDepart();
            this.ajouterItems(5, this.fond.getLargeur() - this.itemJoueur.getSprite().getLargeur(), this.fond.getHauteur() - this.itemJoueur.getSprite().getHauteur());
            this.niveau++;
        }
    }

    public void ajouterItems(int nbItems, int xMax, int yMax) { // Ajoute des items aléatoirement
        Random r = new Random();
        int x, y;

        for (int i = 0; i < nbItems; i++) {
            x = r.nextInt(xMax);
            y = r.nextInt(yMax);
            items.add(new Obstacle("./img/petit_bloc_pierre.png", x, y, 0, 0));
        }
    }
}
